<?php
ini_set('memory_limit','5m');
$begin = microtime(true);

/**
 * @param $className
 */
function __autoload($className) {
	$filename = "./lib/". $className .".php";
	if(file_exists($filename))
		require_once $filename;
}

echo "Begin parsing key-value file\n";
$parser = Parser::create('key-value.log');
$parser->parse();

echo sprintf("Time: %01.2f s\n", microtime(true)-$begin, 2);
echo sprintf("Row parsed: %s \n\n", $parser->getRowNumber()-2);

echo "Begin parsing json file\n";
$begin = microtime(true);
$parser = Parser::create('json-log.json');
$parser->parse();

echo sprintf("Memory usage: %s Mb\n", round(memory_get_peak_usage(true) / (1024 * 1024), 2));
echo sprintf("Time: %01.2f s\n", microtime(true)-$begin, 2);
echo sprintf("Row parsed: %s \n", $parser->getRowNumber()-2);