<?php

class CSVWriter{

	/**
	 * @param $file
	 * @param array $data
	 */
	public static function writeHeader($file,array $data){
		self::prepend(implode(',',$data)."\n",$file);
	}

	/**
	 * @param $file
	 * @param array $data
	 */
	public static function writeRow($file,array $data){
		$out = fopen($file, 'a');
		fputcsv($out, $data);
		fclose($out);
	}

	/**
	 * @param $string
	 * @param $filename
	 */
	private static function prepend($string, $filename){
		$context = stream_context_create ();
		$fp = fopen($filename,'r',1,$context);
		$tmpname = md5($string);
		file_put_contents($tmpname,$string);
		file_put_contents($tmpname,$fp,FILE_APPEND);
		fclose($fp);
		unlink($filename);
		rename($tmpname,$filename);
	}
}