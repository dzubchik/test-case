<?php

class KeyValueParser extends AbstractParser
{
	/**
	 * @var string
	 */
	private $pattern = '/(?P<date>\d{1,2}\/\d{1,2}\/\d{1,4}\s+\d{1,2}:\d{1,2}:\d{1,2}\s+\w{2}\s+)?(?P<ip>\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}\s+)?((?=\s?)(?P<keys>\w+)=(?P<values>([^=]+)(?=\s\w+=)|(\s)|(.*\s?)))+/i';
	/**
	 * @var array
	 */
	private $defaultKeys = array('date', 'ip');

	/**
	 * @param $filename
	 */
	public function __construct($filename)
	{
		return parent::__construct($filename);
	}

	/**
	 * @description Parse key-value string according to pattern
	 */
	public function parse()
	{
		while ($row = $this->readByLine()) {
			if (!preg_match_all($this->pattern, $row, $matches)) {
				continue;
			}

			if ($this->getRowNumber() == 1) // first row
			{
				$this->setHead(array_merge($this->defaultKeys, $matches['keys'])); //initial fields
			}
			$keys = array_merge($this->defaultKeys, $matches['keys']);
			array_unshift($matches['values'], trim($matches['date'][0]), trim($matches['ip'][0]));
			$rowData = array_combine($keys, $matches['values']);
			//hack to remove line break
			end($rowData);
			$last = key($rowData);
			$rowData[$last] = str_replace("\n", "", $rowData[$last]);
			reset($rowData);
			//end hack
			$this->writeRow($this->getSortedRow($rowData,$keys));
		}
		$this->writeHead();
	}
}