<?php

abstract class AbstractParser
{
	/**
	 * @var resourse
	 */
	private $handle;
	/**
	 * @var string
	 */
	private $dataDir;
	/**
	 * @var
	 */
	private $fileName;
	/**
	 * @var int
	 */
	private $rows = 1;
	/**
	 * @var array
	 */
	private $head = array();

	/**
	 * @var string
	 */
	private $csvFile;

	/**
	 * @param $filename
	 * @throws Exception if file does not exist
	 */
	public function __construct($filename)
	{
		$this->fileName = $filename;
		$this->dataDir = dirname(__FILE__) . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . 'data' . DIRECTORY_SEPARATOR;
		if (!is_file($this->dataDir . $this->fileName)) {
			throw new Exception(sprintf('File can not be found in path %s', $this->dataDir . $this->fileName));
		}
		$this->load();
		$this->csvFile = $this->dataDir . $this->fileName . '.csv';
	}

	/**
	 * @return mixed
	 */
	public abstract function parse();

	/**
	 * @param array $head
	 */
	public function setHead(array $head)
	{
		$this->head = $head;
	}

	/**
	 * @return array
	 */
	public function getHead()
	{
		return $this->head;
	}

	/**
	 * @return int
	 */
	public function getRowNumber()
	{
		return $this->rows;
	}

	/**
	 * @description Increase row number
	 */
	private function nextRow()
	{
		$this->rows++;
	}

	/**
	 * @param array $data
	 */
	public function writeRow(array $data)
	{
		CSVWriter::writeRow($this->csvFile, $data);
	}

	/**
	 *
	 */
	public function writeHead()
	{
		CSVWriter::writeHeader($this->csvFile, $this->getHead());
	}

	/**
	 * @return bool|string
	 */
	public function readByLine()
	{
		if ($this->handle) {
			while (!feof($this->handle)) {
				$buffer = fgets($this->handle);
				$this->nextRow();
				return $buffer;
			}
			return false;
		}
		return false;
	}

	/**
	 * @param $rowData
	 * @param $keys
	 * @return array
	 */
	public function getSortedRow($rowData, $keys)
	{
		$diff12 = array_diff($this->getHead(), $keys); // diff between array 1 and 2
		$diff21 = array_diff($keys, $this->getHead()); // diff between array 2 and 1
		foreach ($diff12 as $key) {
			$rowData[$key] = ' ';
		}
		$head = $this->getHead();
		foreach ($diff21 as $key) {
			$tmp = $rowData[$key];
			unset($rowData[$key]);
			$rowData[$key] = $tmp;
			array_push($head, $key);
		}
		$this->setHead($head);
		$sortedRow = array();
		foreach ($this->getHead() as $item) {
			$sortedRow[$item] = $rowData[$item];
		}
		return $sortedRow;
	}

	/**
	 * @throws Exception
	 */
	public function load()
	{
		$this->handle = fopen($this->dataDir . $this->fileName, "r");
		if (!$this->handle) {
			throw new Exception("Couldn't get handle");
		}
	}

	/**
	 * @description Closes file on object destruction
	 */
	function __destruct()
	{
		if ($this->handle) {
			fclose($this->handle);
		}
	}
}