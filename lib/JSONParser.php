<?php

class JSONParser extends AbstractParser
{
	/**
	 * @param $filename
	 */
	public function __construct($filename)
	{
		return parent::__construct($filename);
	}

	/**
	 * @description Parse JSON file
	 */
	public function parse()
	{
		while ($row = $this->readByLine()) {
			$rowData = json_decode($row, true);
			if(json_last_error()){
				continue;
			}
			if ($this->getRowNumber() == 1) // first row
			{
				$this->setHead(array_keys($rowData)); //initial fields
			}
			$keys = array_keys($rowData);
			$this->writeRow($this->getSortedRow($rowData,$keys));
		}
		$this->writeHead();
	}

}