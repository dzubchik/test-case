<?php

/**
 * Class Parser
 * @description Allow to parse data from json and key-value formats
 * @throws Exception if file extension not in list [json, log]
 */
class Parser
{
	public static function create($filename)
	{
		$ext = array_pop(explode('.', $filename));
		switch ($ext) {
			case 'json':
				return new JSONParser($filename);
			case 'log':
				return new KeyValueParser($filename);
/*			case 'xml':
				return new XMLParser($filename);*/
			default:
				throw new Exception('Filename should contain a valid file extension: .json  for JSON file and .log for key-value file');
		}
	}
}